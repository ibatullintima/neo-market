import axios from 'axios'
import Cookies from 'js-cookie'

export const BASE_API_URL = `https://neo-shop-test-one-651f5cf22790.herokuapp.com/api/v1/`

export const axiosRequest = axios.create({
  baseURL: BASE_API_URL,
})

axiosRequest.interceptors.request.use(
  config => {
    const accessToken = Cookies.get('access')

    if (accessToken)
      config.headers.Authorization = `Bearer ${accessToken}`

    return config
  },
  error => Promise.reject(error),
)
