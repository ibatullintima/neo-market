import type { Metadata } from 'next'

import './globals.css'
import Providers from './providers'
import Footer from '@/components/layout/Footer'

export const metadata: Metadata = {
  title: {
    default: 'neo-market',
    template: '%s | neo-market',
  },
  description: 'Online Market Place',
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html>
      <body>
        <Providers>
          {children}
          <Footer/>
        </Providers>
      </body>
    </html>
  )
}
