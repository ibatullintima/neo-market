'use client'

import { useEffect } from 'react'

// import { ChakraProvider } from '@chakra-ui/react'

import Cookies from 'js-cookie'
import { useSnapshot } from 'valtio'

import { Loader } from '@/components/loader/Loader'

import { User } from '@/module/user'
import { UserStore } from '@/store/user'
import { UserStoreActions } from '@/store/user/actions'

if (Cookies.get('access'))
  UserStoreActions.signIn()

const Providers = ({ children }: { children: React.ReactNode }) => {
  const { isAuth } = useSnapshot(UserStore)

  const {
    fetch,
    isFetching,
  } = User.use()

  useEffect(() => {
    if (isAuth) fetch()
  }, [])

  if (isFetching) return <Loader/>

  return (
    <div>
      {children}
    </div>
  )
}

export default Providers
