import { useState } from 'react'

import { axiosRequest } from '@/configs/axios'
import { UserStoreActions } from '@/store/user/actions'
import { UserTypes } from '@/types/user'

export namespace User {
  export const useCurrentUser = () => {
    const [isFetching, setIsFetching] = useState(false)

    const fetch = async () => {
      setIsFetching(true)

      try {
        const response = await axiosRequest.get<UserTypes.Root>('/accounts/users/get_userinfo/')

        if (response.data)
          UserStoreActions.setUser(response.data)
      } finally {
        setIsFetching(false)
      }
    }

    return {
      isFetching,
      fetch,
    }
  }

  export const use = useCurrentUser
}
