export namespace UserTypes {
  export interface Response {
    count: number
    next: string | null
    previous: string | null
    results: Item[]
  }

  export interface Root {
    id: string
    username: string
    image: string
    user_type: string
    rating: number
    description: string | null
    phone_number: string | null
    email: string
    country: {
      title: string
      id: number
    } | undefined
    itn: string | null
    legal_address: string
    director: string | null
    reviews_count: number
    user_status: Usertatus
    user_banner: UserBanner
    social_owner: SocialOwner[]
    posts_count: number
    is_recommended: boolean
  }

  export interface Item {
    id: string
    username: string
    image: string | undefined
    rating: number
    user_type: string
    description: string | undefined
    phone_number: string | undefined
    user_status: Usertatus
    email: string
    country: {
      title: string
      id: number
    } | undefined
    itn: string | undefined
    legal_address: string | undefined
    director: string | undefined
    is_recommended: boolean
  }

  export interface Usertatus {
    id: number
    user: string
    is_agree: boolean
    is_active: boolean
    is_premium: boolean
    premium_activated: string | null
    premium_expired_date: string | null
    password_last_update: string
    created_at: string
  }

  export interface UserBanner {
    id: number
    banner: string
  }

  export interface SocialOwner {
    id: number
    social_type: SocialType
    link: string
    owner: string
  }

  export interface SocialType {
    id: number
    domain_url: string
    logo: string
    title: string
  }

  export interface Form {
    username: string
    email: string
    password: string
    confirm_password: string
    old_password: string
    is_agree: boolean
    phone_number: string
  }
}
