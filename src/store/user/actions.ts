import Cookies from 'js-cookie'

import { UserTypes } from '@/types/user'

import { UserStore } from '.'

export namespace UserStoreActions {
  export const signIn = () => {
    UserStore.isAuth = true
  }

  export const signOut = () => {
    UserStore.isAuth = false

    Cookies.remove('access')
    Cookies.remove('refresh')
  }

  export const setUser = (user: UserTypes.Root) => {
    UserStore.user = user
  }

  export const removeUser = () => {
    UserStore.user = null
  }
}
