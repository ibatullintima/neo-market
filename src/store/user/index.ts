import { proxy } from 'valtio'

import { UserTypes } from '@/types/user'

interface UserStoreTypes {
  isAuth: boolean
  user: UserTypes.Root | null
}

export const UserStore = proxy<UserStoreTypes>({
  isAuth: false,
  user: null,
})
